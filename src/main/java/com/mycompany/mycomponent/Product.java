/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class Product {
    private int id;
    private String name;
    private double price;
    private String imag;

    public Product(int id, String name, double price, String imag) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.imag = imag;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setImag(String imag) {
        this.imag = imag;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getImag() {
        return imag;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", imag=" + imag + '}';
    }
    
    public  static ArrayList<Product>getProductList(){
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1, "Espresso", 40, "1.jpg"));
        list.add(new Product(2, "Americano", 40, "2.jpg"));
        list.add(new Product(3, "Latte", 40, "3.jpg"));
        list.add(new Product(4, "Cappuccino", 40, "4.jpg"));
        list.add(new Product(5, "Coco", 40, "5.jpg"));
        list.add(new Product(6, "Cahthia", 40, "6.jpg"));
        list.add(new Product(7, "Green Tea", 40, "7.jpg"));
        list.add(new Product(8, "Earl Grey tea", 40, "8.jpg"));
        list.add(new Product(9, "Mocha", 40, "9.jpg"));
        list.add(new Product(10, "White Chocolate Mocha", 40, "10.jpg"));
        return list;
    }
}
